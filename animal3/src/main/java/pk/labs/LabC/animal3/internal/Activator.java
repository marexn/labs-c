/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal3.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.logger.Logger;

/**
 *
 * @author st
 */
public class Activator implements BundleActivator{
    
     @Override
    public void start(BundleContext bc) throws Exception {
         Logger.get().log(this, "aktywacja");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        Logger.get().log(this, "dezaktywacja");
    }
}
