/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal3.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author st
 */
public class animal3 implements Animal{
    
    String status;
    
    @Override
    public String getSpecies() {
        return "Wezowate";
    }

    @Override
    public String getName() {
        return "Bartek";
    }

    @Override
    public String getStatus() {
        return "status3";
    }

    @Override
    public void setStatus(String status) {
        this.status =status;
    }
    private PropertyChangeSupport property = new PropertyChangeSupport(this);
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        property.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        property.removePropertyChangeListener(listener);
    }
}
