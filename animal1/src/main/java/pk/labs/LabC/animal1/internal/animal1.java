/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal1.internal;

import java.beans.PropertyChangeListener;
import pk.labs.LabC.contracts.Animal;
import java.beans.PropertyChangeSupport;
/**
 *
 * @author st
 */
public class animal1 implements Animal {
    String status;
    
    @Override
    public String getSpecies() {
        return "Małpa";
    }

    @Override
    public String getName() {
        return "Marcio";
    }

    @Override
    public String getStatus() {
        return "status";
    }

    @Override
    public void setStatus(String status) {
        this.status =status;
    }
    private PropertyChangeSupport property = new PropertyChangeSupport(this);
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        property.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        property.removePropertyChangeListener(listener);
    }
    
}
