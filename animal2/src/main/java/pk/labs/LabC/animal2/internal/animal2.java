/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal2.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author st
 */
public class animal2 implements Animal {
    String status;
    
    @Override
    public String getSpecies() {
        return "gad";
    }

    @Override
    public String getName() {
        return "jojo";
    }

    @Override
    public String getStatus() {
        return "status1";
    }

    @Override
    public void setStatus(String status) {
        this.status =status;
    }
    private PropertyChangeSupport property = new PropertyChangeSupport(this);
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        property.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        property.removePropertyChangeListener(listener);
    }
}
